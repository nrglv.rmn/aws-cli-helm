FROM node:20

RUN apt-get update -q && \
    apt-get install -q -y awscli

WORKDIR /usr/src/helm
ENV VERIFY_CHECKSUM=false
RUN apt-get install tar gzip -y
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN ./get_helm.sh
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN chmod +x kubectl
RUN mv kubectl /usr/local/bin/